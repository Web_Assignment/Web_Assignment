<?php

    define("TITLE", "Team");

    include('includes/arrays.php');
    include('includes/header.php');


    ?>

<div id = "team-members" >

    <h1 align = "center"> Meet The Gamma Gaming Team</h1>

    <?php
        foreach($teamMember as $member)
    {
    ?>
        <div class="member">
            <div class = "container">
            <img src = "images/<?php echo $member["img"]; ?>.jpg" alt=<?php echo $member["name"]; ?> width = "200" height = "200" style = "float: left"">
                <h2><?php echo $member["name"]; ?></h2>
                 <p><?php echo $member["bio"]; ?></p>
                 

            </div>
        </div>
        <?php
            }
         ?>

</div>


<?php
    include('includes/footer.php');

    ?>

