<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="Contact Form" content="">
    <meta name="Chantelle Saliba" content="">
    <title>Contact Form</title>

</head>

<?php

define("TITLE", "Get in touch");
include('includes/header.php');
error_reporting(-1);
ini_set('display_errors', 'On');
set_error_handler("var_dump");

?>

<div id = "form">

    <hr>
    <h1>Get in touch!</h1>

    <?php

    //This is for header injections(preventive for hackers)

    function has_header_injection($str)
    {
        return preg_match("/[\r\n]/", $str);
    }
    if(isset($_POST["Send"])) {
        $name = trim($_POST["name"]);
        $email = trim($_POST["email"]);
        $msg = $_POST["message"];

        if (has_header_injection($name) || has_header_injection($email)) {
            die();
        }

        if (!$name || !$email || !$msg) {
            echo '<h4 class = "error">All fields required. </h4><a href="Form.php" class = "btn btn-lg btn-primary"> Try again! </a>';
            exit;
        }

        $to = "gammagaming25@gmail.com";
        $subject = "Message from site";

        $message  =  "Name: $name\r\n";
        $message .=  "Email: $email\r\n";
        $message .=  "Message: \r\n$msg";
        $message = wordwrap($message, 72);

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/plain; charset=iso-8859-1\r\n";
        $headers .= "From: $name  <$email> \r\n";
        $headers .= "X-Priority: 1\r\n";
        $headers .= "X-MSMail-PRIORITY: High\r\n\r\n";

        $sent = mail($to, $subject, $message, $headers);

        if($sent)
        {
            print "Thanks for contacting us. We will get back to you shortly\n";
        }else
        {
            print("An error occurred and not email was sent\n");
        }

        ?>


        <p><a href = "Mainpage.php" class = "btn btn-lg btn-primary"> &laquo; Home Page</a></p>

    <?php } else { ?>

        <form method="post" action="" id="contact form">

            <label for="name"> Name</label>
            <br>
            <input type="text" id="name" name="name">

            <br>

            <label for="email">Email</label>
            <br>
            <input type="email" id="email" name="email">

            <br>

            <label for="message">What would you like to tell us</label>
            <br>
            <textarea id="message" name="message"></textarea>

            <br>
            <input type="submit" class="btn btn-lg btn-primary" name="Send" value="Submit Message">

        </form>

    <?php } ?>

    <hr>

</div>

<?php
include('includes/footer.php');

?>
